+++
cover = "/uploads/lake_erie_january_2018.jpeg"
date = 2018-01-05T06:00:00Z
slug = "preparation"
title = "Preparation"

+++
This winter has started with record snow and bitter cold. Up until a couple of days ago I have been able to do a daily two mile walk to Lake Erie. This is great physical preparation for walking in India and I have sensed God’s presence in spiritual preparation.

![Lake Erie, January 2018](/uploads/lake_erie_january_2018.jpeg "Lake Erie, January 2018")

Our team has been called to prayer walk the cities of Dehli, Kolkata and Varanasi. In our walking we are to exercise the faith and light God has put in us. Jesus said, “I am the light of the world” and He said, “You are the light of the world.” As an exercise of faith, we will walk in a dark place and bring His light.

![This is our time visiting with the head Imam of Kolkata in 2017](/uploads/visit_with_imam_in_kolkata_.jpeg "This is our time visiting with the head Imam of Kolkata in 2017")