+++
cover = ""
date = 2018-01-05T06:00:00Z
slug = "hello-friends"
title = "Hello Friends"

+++
Hello friends: In an effort to express myself, my experiences and my understanding of God’s story in my life, I am beginning this blog. My hope is to be faithful in communicating something that is helpful in your journey and an encouragement to walk in the light of our Savior.

I am also hopeful to document more significant adventures regarding mission trips abroad and at home. Having always been amazed and humbled by the doors God has opened, this blog is intended to share with family and friends the experiences and associated insight I may gain from week to week.

Thanks for reading and please feel free to share your thoughts.

God’s blessings,

Brian