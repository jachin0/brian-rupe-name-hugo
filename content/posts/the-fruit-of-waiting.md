+++
cover = "/uploads/snowy_mn.jpeg"
date = 2019-04-06T05:00:00Z
draft = true
title = "The Fruit of Waiting"

+++
Last summer at Camp du Nord, we had the pleasure of participating in the after dinner community sing on the front porch. A group of around 50 gathered to harmonize with simple songs and simple meanings. One of my favorites went like this:

Let fruit ripen and let it fall,

Force is not the way at all

Let things go and you will see,

The way to do is to be.

After meditating on this simple tune, I was certain that my Amish friends would have understood this song. Their approach to life seems to deeply personify the message. In the midst of the fast paced world they have no problem taking their buggy to the neighbors to help a young mother with a sick child or leaving a message on a borrowed phone that may not get answered for days. Somehow, they seem to allow life to come to them instead of going after it at a break neck pace.

I remember a young adult scout leader, Steve, in troop 429. All of us impressionable boys thought Steve was really something special. One of our greatest delights was getting to drive with him for a weekend camp out. He would race down the high way at 20 to 30 mph over the speed limit. We would stop for snacks and still beat everyone else to the campsite. It was ironic that we would go at such incredible speeds to a place where we were meant to slow down and enjoy the solitude of creation. Steve told us his life motto was, “to live fast and die young.” Sounded kind of exciting. Steve was only a leader for about nine months. I think the older and wiser adult leaders eventually got wind of his philosophy and un-invited him from the troop.

This winter, Minnesota was overwhelmed with snow. Though not one individual big snowfall, it continued to mount up, week after week and the temperatures remained cold. With the need to throw the snow higher and higher, it became a slower and slower process to clear a way. I needed to spend more time resting and waiting to catch my breath. When the melt finally began, we were worried about flooding and though there were problems in some areas, considering the amount of snow we were fortunate to have a slow melt. The deep snow has the benefit of providing an insulation to the soil and a greater protection from the destructive impact of deep freezing. The slow melt provides greater amounts of moisture to remain in the soil and a steady release of much needed nitrogen that is trapped inside the snow.

Once again, creation speaks to our hearts regarding the ways of the Lord. Psalm 27:14 ends with these words, “Wait for the Lord, be strong, and let your heart take courage; wait for the Lord.” I do not like to wait. I think my favorite phrase as a child was, “I just can’t wait!!!” I want this winter to end, I want this trial to go away, I just wish some things could happen faster. Psalm 27 seems to be saying that the fruit of waiting is strength and courage. Sounds like fruit worth waiting for.

![The snow in our driveway in Coon Rapids, March 1st, 2019](/uploads/snowy_mn.jpeg "The snow in our driveway in Coon Rapids, March 1st, 2019")