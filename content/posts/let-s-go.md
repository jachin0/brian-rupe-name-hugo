+++
cover = "/uploads/adventures_in_saying_yes_to_god.jpg"
date = 2018-01-14T06:00:00Z
title = "Let’s Go"

+++
Off to a great start flying from Cleveland to Erie on the first leg. Lynn just finished reading a book by an old friend, Kel Steiner, who was a big inspiration to us regarding foreign missions. Kel was a leader on the YWAM (Youth With A Mission) Base in Guam and came to the Upper Room in the late 70’s to recruit workers. Many responded to this very likable adventure-loving Kiwi, and went for training and service in foreign missions.

I knew Lynn was enjoying the book and looking for names of our Upper Room friends but I was unaware of the title, until I pulled it out of my backpack on the plane: Adventures in Saying Yes to God. Well, that is interesting since this was pastor Paul’s theme for 2018. Say yes to God! Walking around the airport to get my 10,000 steps in, I suddenly heard my theme song for India, for saying yes to God and for being available to Him, “Let’s Go” by Calvin Miller. I know this is not a Christian song, but God has used it’s simple theme to encourage me. My favorite phrase that I learned last year in Hindi was “Chello”, “Let’s Go”

On the plane I made friends with an Indian man who is a brain scientist at Case Western. I do believe the battleground is in our mind. He has chosen to leave his Hindu faith and become an atheist. All of his great intelligence has provided for him a spiritual vacuum. We must learn to respect our intelligence but lean on the Holy Spirit to help us to let go and let’s go with Jesus.