+++
cover = ""
date = 2018-03-06T06:00:00Z
title = "Earthen Vessel Part II"

+++
![Chai stand in Kolkata](/uploads/chai_vendor_in_kolkata_.jpeg "Chai stand in Kolkata")

![Chai trash bucket](/uploads/used_chai_bucket.jpeg "Chai trash bucket")

Here is our team ordering chai on the street in Kolkata. The other photo is the discard bucket for the clay chai cup. My first chai experience in 2017, I was really fascinated by the cup. It was very small and easily the simplest clay vessel I had ever seen. It was also, in a certain way, my favorite clay vessel. I noticed that the chai vendor had a whole case of cups and by the way they were stacked I could tell that each one was slightly different than the next. At least in some way, they must be hand made.

My chai cost about 6 rupees, which is about 10 cents. I was assuming that I would return the cup so that it could be washed and reused, but when I went to return the cup, I was told to throw it in the trash. How can this be? Maybe they did not want to use a cup after it had been used by a foreigner or maybe I did not understand what he wanted me to do. Surely, he did not want me to throw away the precious cup. My friend familiar with Indian culture said, “throw it in the bucket”, but I said, “I do not want to throw it in the bucket”. He said, “throw it in the bucket and smash it, break it!” But I said, “I would rather keep it than throw it away.” He said, “you must throw it in the bucket and smash it, this is the Indian way; you cannot keep it because it is used and dirty, it is not worth keeping or reusing, it must be thrown away.” So I threw it hard into the bucket and it broke. I cannot remember ever breaking an earthen vessel on purpose and it was kind of fun. But somehow, in this very poor country, it seemed odd to be so extravagant and drink one time out of the chai cup and throw it away. Surely the Lord has a message.

When Gideon was commissioned by God to form an army and defeat an enemy, God wanted to make sure that everyone knew God was the one who would gain the victory. He instructed Gideon to trim down the army to just 300 men and go to battle with each man carrying three items: an earthen vessel, a torch and a trumpet. Rather unusual weapons. They went to battle with the clay vessel covering the torch. At the right moment in time, they blew the trumpets, broke the earthen vessels and cried, “the sword of the Lord and of Gideon.” The enemy of thousands fled and the battle was over. This story can be found in Judges 7.

The treasure we have in earthen vessels is the light of Christ. When we think of what it means to be broken, or humble, we must understand that this is how the victory is won. Others will not be able to see the light unless we break the vessel. Our life in Christ is a great treasure and we must let the light of Christ shine out from our weak earthen vessel, so that others may see and believe.