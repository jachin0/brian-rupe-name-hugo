+++
cover = "/uploads/wild_ass_sanctuary.jpg"
date = 2018-02-06T06:00:00Z
title = "An Interesting Sign In India"

+++
![](/uploads/wild_ass_sanctuary.jpg)

Waiting for a bus in Kolkata, I noticed a sign in bold letters, “ WILD ASS SANCTUARY”. I could not help but laugh out loud and was sure the Lord must have a message for me. My imagination pictured a church sanctuary with a really intense outpouring of the Holy Spirit. At the same time, I felt a little guilty because my mother would never want me to use the “A” word or laugh at someone who did. Sorry mom, on this one I think the Lord wanted to use humor to speak to my heart. I imagined an unbeliever who was at the meeting showing up at a bar late that night and describing his experience at the wild ass sanctuary. Of course, there must be an area somewhere in India where you can go to see wild asses or what we would prefer to call donkeys. The bus station was offering guided trips to this area.

The more I listened to my heart the more I realized that we are all wild asses that need a sanctuary. So every time we come together to worship the Lord we are experiencing a wild ass sanctuary. By the grace of God He has tamed us to be His obedient servants, living stones and part of the living sanctuary not built with human hands but by God Himself. One of the themes of the India trip is the understanding that we are fragile earthen vessels with a great treasure inside. As God changes us from wild asses to patient, gentle donkeys we are fit for His service. Donkeys are such a great picture in God’s creation of an animal that is easily tamed and prepared to serve, to carry another’s burden and to do it patiently, carefuly and gently. In Numbers 22, we see this reality working in Balaams donkey. The gentle donkey responded to his wicked master with truth and humility - a treasure in an earthen vessel.