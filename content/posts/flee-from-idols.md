+++
cover = ""
date = 2018-01-29T06:00:00Z
title = "Flee From Idols"

+++
One of the wonders of India are it’s many languages. Though Hindi and English are prominent, in Kolkata, Bengali is an official language and the Muslim population uses Urdu. Kolkata is in the state of West Bengal and to the east is Bangladesh. Bill, our team leader, will be traveling with Rosia to the state of Nagaland so that Rosia can spend the next month serving at a ministry center before returning to Mexico. Nagaland is one of many states that are a part of India that wrap around the small nation of Bangladesh and boarders Burma. They speak Nagamese in Nagaland and it is more like a jungle(sounds fun). Last night we served in a children’s ministry and the regular service at Munda Para. The Munda people are a separate tribe and speak their own Munda language. It is a wonder that anyone can understand anything.

![This speaker is tied into the rough sawn rafter boards at Munda Para](/uploads/sound_system_at_munda_para.jpeg "This speaker is tied into the rough sawn rafter boards at Munda Para")

![Some of our overcoming friends at Munda Para](/uploads/friends_at_munda_para.jpeg "Some of our overcoming friends at Munda Para")

In our very humble meeting place at Munda Para, was a gathering of the nations and a troop of overcomes(about 30). Represented were the nations of India, including those from Munda and others who spoke Hindi or Bengali. The Latin American nations included, Mexico, Dominican Republic, Argentina and Chili. The U.S. from the state of Pennsylvania was also present and accounted for.

At the end of the service, Javier asked me to share something before communion. I whispered to Bill, “Where is the classic communion verse”, he whispered back, “not sure, check 1 Corinthians 9 or 10”. 1 Corinthians 10:14 jumped out,”. . . flee from idolatry.” Though we all struggle with idolatry in different ways, India is a nation full of literal idols made with human hands and we were with a group of believers who had, by the grace of God, fled from these idols.

Could there be a connection between idol worship and the blood and body of our Lord? As the bread was broken from one loaf I was aware that His heart is to have His body on earth whole. This passage speaks to a priority of attitude we are to have toward the body of Christ. We must do all that we can do to love and serve our brothers and sisters, forgive one another, honor one another, preserving the unity of the Spirit. Our joyful obedience in this regard is the answer to Jesus prayer in John 17. Perhaps we have many religious idols that destroy our unity in Christ. Time to flee them!