+++
cover = ""
date = 2018-01-11T06:00:00Z
title = "A Call To Fruitfulness"

+++
In one day I turn 61 and a couple of days later, I am off to India. The benchmark of a Birthday often leads to some self reflection. This also marks the anniversary of retirement from the YMCA and the end of a busy year that included two glorious weddings, a new grandson, news of soon coming twin grand babies (via Adrienne and Chris in April) and much travel. So glad the Lord gave me the opportunity to be available in this past year. My friend, Paul calls his ministry, “the ministry of availability”. This concept is a big inspiration to me. My pastor, another Paul, in his new year message challenged us to embrace the 8 in 2018 and expect new beginnings(in biblical numerology 8 is the number of new beginnings). He shared, “expect to see open doors and be ready to say ‘yes’ to God.”

Always a good idea to say “yes” to God.In my my waking hours this morning I believe I heard God saying, “I have called you to fruitfulness” and than made it even more clear by challenging me, “you are called to love. You are called to peace, you are called to joy, you are called to patience, you are called to kindness, you are called to goodness, you are called to faithfulness, you are called to gentleness, you are called to self control”. Not sure I ever looked at the fruit of the Spirit as a calling. I can visualize Jesus standing at an open door calling to me, encouraging me to step into my calling to be fruitful. I need to say yes, here I am, send me.

After a very harsh winter storm, today it was 60 degrees and felt like spring, hmm, another sign of a new beginning.