+++
cover = "/uploads/giant.jpeg"
date = 2018-01-19T06:00:00Z
title = "Temples"

+++
All over India there are temples, places of worship. I guess the same as in the US there are so many church buildings, places of worship. It seems interesting that we call church buildings, “churches”, since the church is made up of people, not brick and mortar. The apostles Peter and Paul both used a temple analysis when referring to individual people. 1 Peter 2:5 talks about believers being living stones, not cold, hard lifeless stones but alive in Christ. I love the lyrics of a chorus describing this reality:

We are being built into a temple, Fit for God’s own dwelling place, Into the house of God which is the church, The pillar and the ground of truth, As precious stones that Jesus hones Fashioned by His wondrous grace, And as we love and trust each other, So the building grows and grows.

Somehow, by the grace of God, A temple of living stones is being built together, each stone, unique and uniquely fit next to other stones to form a dynamic team all over the world, to spread the good news, make disciples and see the manifestation of His kingdom, on earth as it is in heaven.

The Father is honing us, shaping us, most often through the imperfections of one another. This is another opportunity to say “yes” to God, yes to his honing and shaping of all of us living stones.

The temples we are experiencing in India are examples of man’s attempt to fulfill a form of worship but missing the real point. I worry that the church temple in the US has the same misguided approach, building structures, forms for worship but missing the real point; Christ in us. . .