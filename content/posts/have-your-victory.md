+++
date = 2020-03-26T02:43:05Z
cover = "/uploads/lake_koronis.JPG"
slug = "have-your-victory"
title = "Have Your Victory"

+++

This was the prayer God had given me at a recent men’s retreat. Fifty of us had gathered the same weekend president Trump had declared a national day of prayer. This being in response to the outbreak of the coronavirus – covid 19. We had gathered as Way of the Lord men at Camp Koronis on Koronis Lake for a men’s retreat planned months earlier, not realizing we would be in prayer regarding the coronavirus that very weekend. These things do not just happen by coincidence. This was a divine appointment – this is the way of the Lord.

As each one walked to the front of the chapel to offer prayer as God was leading them, I sensed an impression regarding the K in Lake Koronis. Corona is Latin for crown and Korone' is the Greek version of the same. The Coronavirus apparently looks like a crown, thus it’s name. But my understanding is that crowns are for kings, not for viruses and our Lake Koronas with a K was confirmation that the King of Kings was in charge of the situation. Thus my prayer, “King of Kings, have your victory!”

Before I walked up to pray, I asked the Lord for further confirmation that I needed to make this a public declaration. Behind me came a tap on the shoulder from one of our leaders, Larry Alberts, and a whisper in the ear, “I think you have something that needs to be prayed.” I smiled, said “thank you” and walked to the front.

The next day I received a text from my friend Mike McCabe, confirming the declaration and encouraging a blog post on the subject. As I write the post, two additional thoughts began to surface. The first being how the Lord delights in our willingness to wait upon Him, hear His voice and obey. This is what faith looks like. I John 5:4 reads, “This is the victory that overcomes the world, even our faith.”

The second thought is a principle of honoring. Larry and Mike’s willingness to listen to the Holy Spirit and encourage another’s gift is how body life is supposed to work. When we honor one another’s giftings and encourage their use for the edification of the Body of Christ, really good things happen. Bethel Church in Redding California goes as far as to say that this honoring of one another is key for the release of healing and miracles. Indeed, honoring is an expression of unity. Where there is unity, God commands a blessing.

This is the victory that God WILL have and we have a critical part to play in its manifestation.