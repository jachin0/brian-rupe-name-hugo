+++
cover = "/uploads/chai.jpeg"
date = 2018-03-01T06:00:00Z
title = "Earthen Vessels Part One"

+++
![Clay chai cup with chai](/uploads/chai.jpeg "Clay chai cup with chai")

This Sunday, March 4th, I will be marching forth at New Beginnings Church in Fairview to share some of my experiences in India. Anyone interested is certainly welcome to our 10:00 a.m. service. Part of my story will be an explanation of the clay chai cup. My introduction in this blog will be continued this Sunday morning. Please pray that I will have God’s ability as I share.

The clay chai cup is a prophetic wonder in India. On the streets of Kolkata, stopping for a cup of chai is a common daily practice. The best way to drink Chai is freshly made, on the street in a clay cup. Chai is made of milk, tea, sugar and spice. When you drink it out of the clay cup you can smell the earthy clay adding the final touch of drinking ecstasy.

I am not normally a tea or coffee drinker, however, when in Rome . . ., so I partook last year for the first time. It was good and fun to drink with my friends. It is handed to you just about one degree less than boiling and the heat quickly penetrates the thin clay cup and burns your hand. You quickly learn that it is necessary to carefully hold the cup with your thumb on the edge of the rim and another finger on the edge of the base. Allow plenty of time for the hot creamy mixture to cool down and enjoy the sweet earthy spicy delight.

Why do I say that this cup is a prophetic wonder? Simply because there are multiple Kingdom messages that this precious cup is trying to tell us. In 2 Corinthians 4:7 we read, “But we have this treasure in earthen vessels, that the excellence of the power may be of God and not of us.” What is the treasure? In the previous verse we learn what the treasure is. Light shining out of darkness, shining in our hearts to give the light of the knowledge of the glory of God in the face of Jesus Christ. Verse 6 is a real mouthful but it must be so, because the treasure is so special, so important and so powerful. The treasure is light, knowledge, God’s glory and His presence. All of this inside of us, inside of this weak clay vessel.

We will continue to explore the wonder of the clay pot and the treasure inside this Sunday and in future blog posts to come.