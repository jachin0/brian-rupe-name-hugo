+++
cover = "/uploads/rickshaw_driver.jpeg"
date = 2018-01-22T06:00:00Z
title = "Honoring the Rickshaw Drivers"

+++
Pastor Michael comes from a high cast family in India and became a Christian many years ago through the ministry of YWAM(Youth With A Mission). He pastors a church in Kolkata and oversees an NGO(non government organization) called “Serve the Nations.” Though it is rare, in the official government paperwork for this NGO, it states that they have permission to teach the Christian faith.

![](/uploads/rickshaw_driver.jpeg)

This NGO has many outreach programs and we had the opportunity to experience this great event on a warm Sunday afternoon at a small park in downtown Kolkata. The Rickshaw Race included about 50 drivers of all ages who were set up in teams. There were team winners and an individual champion. As part of the festivities our clown team performed four skits to the racers and the crowd in attendance.

![](/uploads/clown_team_india.jpeg)

I asked Pastor Michael to describe for me the purpose of the race. He explained that very few people really appreciate the service of the Rickshaw drivers and he believed God wanted him to organize the event as a way to honor the drivers. Only one driver he knows is a Christian believer, most are Hindus. It was very special to see the joy on the faces of these hard working salty men as they participated and received gifts of thanks. They seemed to really enjoy our clown skits as well. I never imagined in my life that I would have opportunity to honor the Rickshaw drivers of Kolkata.

Every person God made is very special. Each of us have a unique calling and destiny to fufill. The Rickshaw Race was a huge reminder to me of the uniqueness of every individual and the value to be honored and respected in all. It is so critical as followers of Jesus that we elevate (honor) every person, created by our loving Heavenly Father with a purpose and destiny to fufill.